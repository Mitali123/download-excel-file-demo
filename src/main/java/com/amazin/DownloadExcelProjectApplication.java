package com.amazin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DownloadExcelProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DownloadExcelProjectApplication.class, args);
	}

}
