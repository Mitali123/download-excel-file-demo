package com.amazin.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.amazin.service.ExcelService;
@CrossOrigin("http://localhost:8081")
@Controller
@RequestMapping("/api/excel")
public class ExcelController {
	@Autowired
	  ExcelService fileService;
	  @GetMapping("/download")
	  public HttpEntity<InputStreamResource> getFile() {
	    String filename = "excel.xls";
	    InputStreamResource file = new InputStreamResource(fileService.loading());
	    return ResponseEntity.ok()
	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
	        .contentType(MediaType.parseMediaType("desktop/mitali.ms-excel"))
	        .body(file);
	  }

}
