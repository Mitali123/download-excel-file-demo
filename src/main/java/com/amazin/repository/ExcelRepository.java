package com.amazin.repository;

import org.springframework.data.repository.CrudRepository;

import com.amazin.model.Excel;

public interface ExcelRepository extends CrudRepository<Excel,Integer>{

}
