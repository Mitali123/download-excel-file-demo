package com.amazin.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class Excel {
	@Id
	private int id;
	private String name;
	private String password;
	private String city;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Excel(int id, String name, String password, String city) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.city = city;
	}
	@Override
	public String toString() {
		return "Excel [id=" + id + ", name=" + name + ", password=" + password + ", city=" + city + "]";
	}
	public Excel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
