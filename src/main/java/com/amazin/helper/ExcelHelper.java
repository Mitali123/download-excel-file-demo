package com.amazin.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.amazin.model.Excel;

public class ExcelHelper {
	public static String TYPE = "desktop/mitali.openxmlformats-officedocument.spreadsheetml.sheet";
	  static String[] HEADERs = { "id", "name", "password", "city" };
	  static String SHEET = "Tutorials";
	  //List<Excel> excel = new ArrayList<>();
	  public static ByteArrayInputStream userToExcel(List<Excel> excel2) {
	    try (HSSFWorkbook workbook = new HSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
	    	HSSFSheet sheet = workbook.createSheet("SHEET");   
	    	//creating the 0th row using the createRow() method  
	    	HSSFRow rowhead = sheet.createRow((short)0); 
	      for (int col = 0; col < HEADERs.length; col++) {
	        Cell cell = rowhead.createCell(col);
	        cell.setCellValue(HEADERs[col]);
	      }
	      int rowIdx = 1;
	      for (Excel excel1 : excel2) {
	        Row row = sheet.createRow(rowIdx++);
	        row.createCell(0).setCellValue(excel1.getId());
	        row.createCell(1).setCellValue(excel1.getName());
	        row.createCell(2).setCellValue(excel1.getPassword());
	        row.createCell(3).setCellValue(excel1.getCity());
	      }
	        
	      workbook.write(out);
	      return new ByteArrayInputStream(out.toByteArray());
	    } catch (IOException e) {
	    	throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
	    }
	  }
	
	    }


