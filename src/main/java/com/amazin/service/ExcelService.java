package com.amazin.service;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazin.helper.ExcelHelper;
import com.amazin.model.Excel;
import com.amazin.repository.ExcelRepository;
@Service
public class ExcelService {
	@Autowired
	  ExcelRepository repository;
	  public ByteArrayInputStream loading() {
	    Iterable<Excel> excel2 = repository.findAll();
	    ByteArrayInputStream in = ExcelHelper.userToExcel((List<Excel>) excel2);
	    return in;
	  }
	}


